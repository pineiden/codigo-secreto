f1='primero.txt'
f2='segundo.txt'

lista_a = []
lista_b = []

with open(f1,'r') as f:
    for line in f:
        lista_a.append(line)

with open(f2,'r') as f:
    for line in f:
        lista_b.append(line)


msg = []
for i, line in enumerate(lista_a):
    s_a = set(line)
    line_b = lista_b[i]
    s_b = set(line_b)
    char = list(s_a - s_b)[0]
    msg.append(char)

total = ''.join(msg)

print("=======MENSAJE======")
print(total)
