import random

def format_up(x):
    return x.upper()

def format_lower(x):
    return x.lower()

def get_set(x):
    assert len(x)==1, "No corresponde"
    value = ord(str(x))
    values = set(range(32,127))
    values.remove(value)
    return values

def random_except(x):
    values = list(get_set(x))
    largo =  random.randint(0,128)
    rand_int = lambda d, l: random.randint(0,l)
    largo_v = len(values)
    unicode_list = [values[rand_int(i, largo_v-1)] for i in range(largo)]
    code_list = list(map(chr, unicode_list))
    conjunto = set(code_list)
    new_place = rand_int(x, len(code_list))
    code_list.insert(new_place, x)
    code_list.append('\n')
    code = ''.join(code_list)
    return code, values, conjunto 

def place_set(values, conjunto):
    largo =  random.randint(0, 128)
    rand_int = lambda d, l: random.randint(0, l)
    largo_v = len(values)
    unicode_list = [values[rand_int(i, largo_v-1)] for i in range(largo)]
    code_list = list(map(chr, unicode_list))
    for elem in conjunto:
        largo = len(code_list)
        new_place = rand_int(elem, largo)
        code_list.insert(new_place, elem)
    code_list.append('\n')
    return ''.join(code_list)
    
def generate_files(msg):
    file_a = []
    file_b = []
    for x in msg:
        code_a, values, conjunto = random_except(x)
        code_b = place_set(values, conjunto)
        print(set(code_a)-set(code_b))
        file_a.append(code_a)
        file_b.append(code_b)
    msg = []
    return file_a, file_b

def create_files(msg, file_path_1, file_path_2):
    file_a, file_b = generate_files(msg)
    with open(file_path_1, 'w') as f:
        for line in file_a:
            f.write(line)
        f.close()
    with open(file_path_2, 'w') as f:
        for line in file_b:
            f.write(line)
        f.close()
    return "Files {0} and {1} created".format(file_path_1, file_path_2)

